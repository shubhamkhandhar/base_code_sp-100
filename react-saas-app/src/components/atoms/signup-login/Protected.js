import React from "react";
import { useEffect } from "react";
import { useNavigate } from "react-router-dom";

export default function Protected(props) {
  let Component = props.Component;
  const navigate = useNavigate();

  useEffect(() => {
    if (!localStorage.getItem("user-key")) {
      navigate("/login");
    }
  }, []);

  return (
      <Component />
  );
}