import React from "react";
import { ClientAdminSidebar } from "./sidebar/ClientAdminSidebar.js";
import "../../assets/css/layout.css";
import { ClientAdminTopNav } from "./topnav/ClientAdminTopNav.js";

export const ClientAdminLayout = (props) => {
  return (
    <div className="layout">
      <ClientAdminSidebar {...props} />
      <div className="layout__content">
        <ClientAdminTopNav />
        <div className="layout__content-main">
          <h3>Welcome to admin client dashboard</h3>
        </div>
      </div>
    </div>
  );
};
