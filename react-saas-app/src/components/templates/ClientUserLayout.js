import React from "react";
import { ClientUserSidebar } from "./sidebar/ClientUserSidebar.js";
import "../../assets/css/layout.css";
import { ClientUserTopNav } from "./topnav/ClientUserTopNav.js";

export const ClientUserLayout = (props) => {
  return (
    <div className="layout">
      <ClientUserSidebar {...props} />
      <div className="layout__content">
        <ClientUserTopNav />
        <div className="layout__content-main">
          <h3>Welcome to user client dashboard</h3>
        </div>
      </div>
    </div>
  );
};
