import React from "react";
import { useNavigate } from "react-router-dom";
import AppBar from "@mui/material/AppBar";
import Box from "@mui/material/Box";
import Toolbar from "@mui/material/Toolbar";
import Typography from "@mui/material/Typography";
import Button from "@mui/material/Button";

export default function Header() {
  const navigate = useNavigate();
  let currentuser = JSON.parse(localStorage.getItem("user-key"));

  function logout() {
    localStorage.clear();
    navigate("/login");
  }

  return (
    <div>
      <Box sx={{ flexGrow: 1 }}>
        <AppBar position="static">
          {localStorage.getItem("user-key") ? (
            <Toolbar>
              <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
                Welcome {currentuser?.username} to NewsData API
              </Typography>
              <Button color="inherit" onClick={logout}>
                Log Out
              </Button>
            </Toolbar>
          ) : null}
        </AppBar>
      </Box>
    </div>
  );
}
