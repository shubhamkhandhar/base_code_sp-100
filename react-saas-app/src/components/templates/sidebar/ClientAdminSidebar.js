import React from "react";
import "../../../assets/css/sidebar.css";
import mitkatLogo from "../../../assets/images/MitKat_new_logo.png";
import sidebar_items from "../../../data/ClientAdminSidebarRoutes.json";
import { Link } from "react-router-dom";

const SidebarItem = (props) => {
  return (
    <div className="sidebar__item">
      <div className="sidebar__item-inner">
        <i className={props.icon}></i>
        <span>{props.title}</span>
      </div>
    </div>
  );
};

export const ClientAdminSidebar = (props) => {
  return (
    <div className="sidebar">
      <div className="sidebar__logo">
        <img src={mitkatLogo} alt="Mitkat Advisory" />
      </div>
      {sidebar_items.map((item, index) => (
        <Link to={item.route} key={index}>
          <SidebarItem title={item.display_name} icon={item.icon} />
        </Link>
      ))}
    </div>
  );
};
