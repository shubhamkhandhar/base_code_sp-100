import React from "react";
import Dropdown from "../../molecules/dropdown/Dropdown";
import user_image from "../../../assets/images/favicon.png";
import user_menu from "../../../data/ClientUserMenus.json";
import { Link } from "react-router-dom";
import "../../../assets/css/topnav.css";

const renderUserToggle = (user) => (
  <div className="topnav__right-user">
    <div className="topnav__right-user__image">
      <img src={user.image} alt="" />
    </div>
    <div className="topnav__right-user__name">{user.display_name}</div>
  </div>
);

const renderUserMenu = (item, index) => (
  <Link to={item.route} key={index}>
    <div className="grid-item">
      <i className={item.icon}></i>
      <span>{item.content}</span>
    </div>
  </Link>
);

export const ClientUserTopNav = () => {
  let currentuser = JSON.parse(localStorage.getItem("user-key"));
  const curr_user = {
    display_name: currentuser.username + " " + currentuser.lastname,
    image: user_image,
  };
  return (
    <div className="topnav">
      <div className="topnav__search">
        <input type="text" placeholder="Search here..." />
        <i className="bx bx-search" />
      </div>
      <div className="topnav__right">
        <div className="topnav__right-item1">
          <Dropdown customToggle={() => renderUserToggle(curr_user)} />
        </div>
        <div className="topnav__right-item2">
          <Dropdown
            icon="bx bxs-grid"
            contentData={user_menu}
            renderItems={(item, index) => renderUserMenu(item, index)}
          />{" "}
        </div>
      </div>
    </div>
  );
};
