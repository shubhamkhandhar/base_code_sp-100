const baseUrl =
  "http://ec2-3-110-154-227.ap-south-1.compute.amazonaws.com:5000";

let apiEndPoints = {
  loginClient: "api/auth/login",
  signupClient: "api/auth/signup",
};

export const API_ROUTES = {
  loginClient: `${baseUrl}/${apiEndPoints.loginClient}`,
  signupClient: `${baseUrl}/${apiEndPoints.signupClient}`,
};
