export function fetchApi(params) {
    let reqUrl = params.url;
    let reqObj = {
      method: params.method,
      mode: "cors",
      cache: "default",
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json",
      },
      ...(params.data && {
        body: JSON.stringify(params.data ? params.data : ""),
      }),
    };
  
    return fetch(reqUrl, reqObj);
  }