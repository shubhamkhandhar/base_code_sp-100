import "./App.css";
import Signup from "./pages/Signup.js";
import * as React from "react";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import Login from "./pages/Login.js";
import ClientUserDashboard from "./pages/ClientUserDashboard";
import Protected from "./components/atoms/signup-login/Protected.js";
import ClientAdminDashboard from "./pages/ClientAdminDashboard";

function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Routes>
          <Route index element={<Login />} />
          <Route path="/login" element={<Login />} />
          <Route path="/signup" element={<Signup />} />
          <Route
            path="/clientUserDashboard"
            element={<Protected Component={ClientUserDashboard} />}
          />
          <Route
            path="/clientAdminDashboard"
            element={<Protected Component={ClientAdminDashboard} />}
          />
          <Route path="*" element={<Login />} />
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
