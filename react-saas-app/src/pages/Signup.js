import React from "react";
import { useState } from "react";
import { useNavigate, Link } from "react-router-dom";
import SignupFormElements from "../data/SignupFormElements.json";
import { TextField, Grid, Paper, Typography } from "@mui/material";
import { Button, StylesProvider } from "@material-ui/core";
import mitkatLogo from "../assets/images/logo.png";
import { fetchApi } from "../helpers/fetchApi.js";
import { API_ROUTES } from "../helpers/apiRoutes";
import "../assets/css/signup.css";

export default function Signup() {
  localStorage.clear();
  const navigate = useNavigate();
  const [state, setState] = useState({
    username: "",
    password: "",
    email: "",
    firstname: "",
    lastname: "",
    address: "",
    contactno: "",
  });

  async function clientSignup() {
    const {
      username,
      password,
      email,
      firstname,
      lastname,
      address,
      contactno,
    } = state;

    const reqObj = {
      username,
      password,
      email,
      firstname,
      lastname,
      address,
      contactno,
    };

    let params = {
      url: API_ROUTES.signupClient,
      method: "POST",
      data: reqObj,
    };

    let result = await fetchApi(params);
    const json = await result.json();
    if (json.message === "Registration Completed successfully!") {
      alert(
        "Registration Completed successfully! Kindly Login with your Credentials"
      );
      navigate("/login");
    } else {
      alert(json.message);
    }
  }

  function handleInputs(event) {
    const { name, value } = event.target;
    setState((prevState) => ({ ...prevState, [name]: value }));
  }

  return (
    <StylesProvider injectFirst>
      <div className="signup__bg__image">
        <form>
          <Grid
            container
            spacing={0}
            direction="column"
            alignItems="center"
            justifyContent="center"
            style={{ minHeight: "100vh" }}
          >
            <Paper elevation={10} className="signup__paper__style">
              <h2>NewsData Feed Signup</h2>
              <Grid direction="row" container spacing={1}>
                <Grid item sm={6}>
                  {SignupFormElements.slice(0, 3).map((user) => (
                    <TextField
                      style={{ marginBottom: 20 }}
                      name={user.value}
                      label={user.label}
                      value={state[`${user.value}`]}
                      fullWidth
                      required
                      type={user.type}
                      placeholder={user.placeholder}
                      variant={user.variant}
                      onChange={(e) => handleInputs(e)}
                    />
                  ))}
                </Grid>
                <Grid item sm={6}>
                  {SignupFormElements.slice(4, 7).map((user) => (
                    <TextField
                      style={{ marginBottom: 20 }}
                      name={user.value}
                      label={user.label}
                      value={state[`${user.value}`]}
                      fullWidth
                      required
                      type={user.type}
                      placeholder={user.placeholder}
                      variant={user.variant}
                      onChange={(e) => handleInputs(e)}
                    />
                  ))}
                </Grid>
                <Grid item sm={12}>
                  {SignupFormElements.slice(3, 4).map((user) => (
                    <TextField
                      style={{ marginBottom: 20 }}
                      name={user.value}
                      label={user.label}
                      value={state[`${user.value}`]}
                      fullWidth
                      required
                      type={user.type}
                      placeholder={user.placeholder}
                      variant={user.variant}
                      onChange={(e) => handleInputs(e)}
                    />
                  ))}
                </Grid>
              </Grid>
              <Button onClick={clientSignup} className="signup__Button">
                Sign Up
              </Button>
              <Typography className="signup__already__have__account">
                Already have an account ? <Link to="/login">Login</Link>
              </Typography>
              <p className="signup__powered__by">Powered by</p>
              <img
                className="signup__mitkat__logo"
                src={mitkatLogo}
                alt="Mitkat Advisory"
              />
            </Paper>
          </Grid>
        </form>
      </div>
    </StylesProvider>
  );
}
