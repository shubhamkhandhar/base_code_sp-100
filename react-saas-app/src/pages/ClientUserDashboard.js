import React from "react";
import { ClientUserLayout } from "../components/templates/ClientUserLayout.js";
import "../assets/boxicons-2.0.7/css/boxicons.min.css";
import "../assets/css/grid.css";
import "../assets/css/index.css";

export default function ClientUserDashboard() {
  return (
      <ClientUserLayout />
  );
}