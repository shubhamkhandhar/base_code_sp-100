import React from "react";
import { ClientAdminLayout } from "../components/templates/ClientAdminLayout.js";

export default function ClientAdminDashboard() {
  return (
      <ClientAdminLayout />
  );
}
