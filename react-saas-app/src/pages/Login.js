import React from "react";
import { useState } from "react";
import { useNavigate, Link } from "react-router-dom";
import LoginFormElements from "../data/LoginFormElements.json";
import { TextField, Grid, Paper, Typography } from "@mui/material";
import { Button, StylesProvider } from "@material-ui/core";
import mitkatLogo from "../assets/images/logo.png";
import { fetchApi } from "../helpers/fetchApi.js";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
import { API_ROUTES } from "../helpers/apiRoutes";
import "../assets/css/login.css";

export default function Login() {
  localStorage.clear();
  const navigate = useNavigate();
  const [state, setState] = useState({ username: "", password: "" });

  // Uncomment this after validation in Signup
  // useEffect(() => {
  //   if (localStorage.getItem("user-key")) {
  //     navigate("/clientUserDashboard");
  //   }
  // }, []);

  async function clientLogin() {
    let reqObj = {
      username: state.username,
      password: state.password,
    };

    let params = {
      url: API_ROUTES.loginClient,
      method: "POST",
      data: reqObj,
    };

    let result = await fetchApi(params);
    const json = await result.json();
    localStorage.setItem("user-key", JSON.stringify(json));
    if (json.roles[0] !== "ROLE_USER") {
      navigate("/ClientAdminDashboard");
    } else {
      navigate("/clientUserDashboard");
    }
  }

  function handleInputs(event) {
    const { name, value } = event.target;
    setState((prevState) => ({ ...prevState, [name]: value }));
  }

  return (
    <StylesProvider injectFirst>
      <div className="login__bg__image">
        <form>
          <Grid
            container
            spacing={0}
            direction="column"
            alignItems="center"
            justifyContent="center"
            style={{ minHeight: "100vh" }}
          >
            <Paper elevation={10} className="login__papers__style">
              <h2>NewsData Feed Login</h2>
              {LoginFormElements.map((user) => (
                <TextField
                  style={{ marginBottom: 20 }}
                  name={user.value}
                  label={user.label}
                  value={state[`${user.value}`]}
                  fullWidth
                  required
                  type={user.type}
                  placeholder={user.placeholder}
                  variant={user.variant}
                  onChange={(e) => handleInputs(e)}
                />
              ))}
              <span>
                <FormControlLabel
                  className="login__form__checkbox"
                  control={<Checkbox name="checkedB" color="primary" />}
                  label="Remember me"
                />
                <Typography
                  style={{
                    paddingLeft: 200,
                    paddingBottom: 20,
                    marginTop: -45,
                  }}
                >
                  <Link to="/">Forgot password ?</Link>
                </Typography>
              </span>
              <Button onClick={clientLogin} className="Login__btn">
                Login
              </Button>
              <Typography className="login__dont__have__account">
                Don't have an account ? <Link to="/signup">Signup</Link>
              </Typography>
              <div style={{ display: "flex", justifyContent: "center" }}>
                <p className="login__powered__by">Powered by </p>
                <img
                  className="login__mitkat__logo"
                  src={mitkatLogo}
                  alt="mitkat"
                />
              </div>
            </Paper>
          </Grid>
        </form>

        {/* For Validation part  
        
        <form>
        <Grid>
          {LoginFormElements.map((user) => {
            const {label, value, placeholder } = user;
            return (
              <TextField
                name={value}
                label={label}
                placeholder={placeholder}
                value={state[`${value}`]}
                onChange={(e) => handleInputs(e)}
              />
            )
          })}
        </Grid>
      </form> */}
      </div>
    </StylesProvider>
  );
}